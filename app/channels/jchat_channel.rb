class JchatChannel < ApplicationCable::Channel
  def subscribed
    stream_from "jchat_channel"
  end

  def unsubscribed
  end
end
