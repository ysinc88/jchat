class Message < ApplicationRecord
  belongs_to :person

  def display
    "(#{self.created_at}) #{self.person.email}: #{self.content}"
  end

  def append
    self.display.prepend('<li>').concat('</li>')
  end
end
