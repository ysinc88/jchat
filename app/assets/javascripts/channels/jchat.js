App.jchat = App.cable.subscriptions.create("JchatChannel", {
  connected: function() {
    console.log('Actioncable Connected');
  },
  disconnected: function() {},
  received: function(data) {
    document.getElementById('content').value = "";
    var target = document.getElementById('message_list');
    target.innerHTML += data;
  }
});
