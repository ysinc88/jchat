class PagesController < ApplicationController
  def landing
    redirect_to action: 'chat' if current_person
  end

  def chat
    redirect_to action: 'landing' if !current_person
    @messages = Message.all
  end
end
