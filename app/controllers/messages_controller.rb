class MessagesController < ApplicationController
  
  def create
    person, content = params[:person_id], params[:content]
    puts [person,content]
    message = Message.create(person_id: person, content: content)
    update_message_list(message.append)
    head :ok
  end

  def update_message_list(message)
    ActionCable.server.broadcast("jchat_channel", message)
  end
end
