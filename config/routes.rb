Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  scope :message do
    get '/list',    to: 'messages#list'
    post '/create', to: 'messages#create'
  end

  devise_for :people
  get 'chat', to: 'pages#chat'

  root to: 'pages#landing'

end
