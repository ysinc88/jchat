require_relative 'boot'
require 'rails/all'
Bundler.require(*Rails.groups)
module Jchat
  class Application < Rails::Application
    config.load_defaults 5.2
    config.generators do |g|
        g.test_framework  false
        g.view_specs      false
        g.helper_specs    false
        g.assets          false
        g.helper          false
        g.jbuilder        false
    end
  end
end
